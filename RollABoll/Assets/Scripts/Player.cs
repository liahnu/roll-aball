using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public Rigidbody rd;
    public int scrore = 0;
    public Text scoreText;
    public GameObject winText;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start");
        rd = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Game are working");
        float h = Input.GetAxis("Horizontal");
        float w = Input.GetAxis("Vertical");
        rd.AddForce( new Vector3(h,0,w));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "point")
        {
            Destroy(other.gameObject);
            scrore++;
            Debug.Log(scrore);
            scoreText.text = "������"+scrore;

            if (scrore == 4)
            {
                winText.SetActive(true);
            }
        }
    }

}
